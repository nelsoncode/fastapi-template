# FastAPI Framework

---

Example project using the [FastAPI](https://fastapi.tiangolo.com/), server and tests

---

## Virtual Enviroment

```bash
virtualenv venv
source ./venv/bin/activate
```

## Install Dependencies

```bash
pip install -r requirements.txt
```

## Run Server

```bash
cd app/ && uvicorn main:app --reload
```

## Tests

```bash
pip install pytest requests
```

```bash
pytest
```