from fastapi import FastAPI
from fastapi.responses import JSONResponse

app = FastAPI()


@app.get("/")
def main():
    return JSONResponse(content={"message": "Hello with FastAPI"}, status_code=200)
